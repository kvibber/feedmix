#!/usr/bin/python
#####################################################################
# feedmix.py
# Version 0.2.1
# by Kelson Vibber, <kvibber.com>
# Free to use under the terms of the BSD 2-clause license (see LICENSE)
# 
# Combines multiple RSS or ATOM feeds into one feed suitable
# for subscribing to all at once. (For example, a full-site feed
# built up of feeds from its different sections.)
# 
# Described at https://hyperborea.org/tech-tips/combine-feeds/
#
# Uses feedparser to read the sources and feedgen to write the output.
#
# Feedparser: https://github.com/kurtmckee/feedparser
#    Used under the terms of the 2-clause BSD license
#    (c) 2010-2022 Kurt McGee and 2002-2008 Mark Pilgrim
#
# Feedgen: https://feedgen.kiesow.be/
#    Used under the terms of the 2-clause BSD license
#    (c) 2011 Lars Kiesow
#
# Helpful docs: https://feedparser.readthedocs.io/en/latest/
#
# HOWTO:
# 1. Install feedparser and feedgen. (Or use your system package if there is one.)
#       pip3 install feedparser
#       pip3 install feedgen
# 2. Put the URLs for the feeds you want to combine into sources.
# 3. Fill in the feed properties.
# 4. Set the output filenames if you don't want latest.html and feed.xml.
#      Feel free to remove the indieweb/microformats2 part if you don't need it
# 5. Run the script.
# 
#####################################################################

import feedparser
import time
from feedgen.feed import FeedGenerator

#####################################################################
# Collect the source feeds
#####################################################################
# CONFIG: Set the URLs of the feeds you're pulling from.
sources = [
  'https://example.com/feed/',
  'https://anotherexample.com/path/to/feed.xml'
]

# Get each source and append all items to the full list
fullList = []
for url in sources:
  feed = feedparser.parse(url)
  for item in feed['items']:
    fullList.append(item)

# Sort by the parsed date, descending, and take the first 15 items.
fullList.sort(key=lambda item: item['updated_parsed'], reverse=True)
outList = fullList [0:15]

#####################################################################
# Build rss/atom feed
#####################################################################
# CONFIG: Set these values for your output feed!
fg = FeedGenerator()
fg.id('WEBSITE_URL')
fg.title('FEED_TITLE')
fg.subtitle('FEED_SUBTITLE')
fg.author( {'name':'MAIN_AUTHOR_NAME', 'email':'MAIN_AUTHOR_EMAIL'} )
fg.link( href='WEBSITE_URL',rel='alternate' )
fg.link( href='FEED_URL', rel='self' )
fg.language('en')

for item in outList:
  fe = fg.add_entry(order='append')
  fe.id(item['id'])
  fe.title(item['title'])
  fe.link(href=item['link'])
  fe.updated(item['updated'])
  # add author if present in original
  if('author' in item):
    # TODO add HREF if available.
    if('email' in item['author_detail']):
      fe.author(name=item['author_detail'].name, email=item['author_detail'].email)
    else:
      fe.author(name=item['author_detail'].name)
  if('summary' in item):
    # If the summary contains HTML code, set its type.
    summary_type = 'html' if '<' in item['summary'] else None
    fe.summary(summary=item['summary'], type=summary_type)
  if ('tags' in item):
    for tag in item.tags:
      fe.category(term=tag.term)
  for content in item.content:
    fe.content(content=content.value, type='html')
fg.atom_file('feed.xml')

#####################################################################
# Build and write IndieWeb-suitable microformats2 feed in latest.html
#####################################################################
indiefeed = []
indiefeed.append("<ul class='h-feed'>\n")
for item in outList:
  shortdate = time.strftime('%b %d', item['updated_parsed'])
  fulldate = time.asctime(item['updated_parsed'])
  indiefeed.append("<li class='h-entry'><time class='dt-updated' datetime='"+ fulldate + "'>" + shortdate + "</time>: <a class='u-url p-name' href='" + item['link'] + "'>" + item['title'] + "</a></li>\n")
indiefeed.append("</ul>\n")

with open('latest.html', 'w') as LatestFile:
  LatestFile.writelines(indiefeed)

